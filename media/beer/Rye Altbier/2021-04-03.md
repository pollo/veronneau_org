# Rye Altbier

A mix between Beau's altbier and Les Grands Bois' Roggenbier.

Brewed as the fist beer of a parti-gyle comprised of:

1. Rye Altbier
2. Kveik Black IPA

## Ingredients

### Fermentables

| Name                        | Amount    |
|---------------------------- |---------- |
| Innomalt Pils Traditionnel  | 1.6 kg    |
| Maltbroue Seigle Malté      | 1.6 kg    |
| Canada Maltage Munich Malt  | 600 g     |
| Gambrinus Dark Munich Malt  | 350 g     |
| Weyermann CaraAroma         | 120 g     |

### Hops

| Name                 | Amount  | Form        | Use           |
|--------------------- |-------- | ----------- | ------------- |
| Perl                 | 28 g    | Pellet      | Boil, 60 mins |
| Perl                 | 14 g    | Pellet      | Boil, 40 mins |
| Hallertau Mittlefruh | 20 g    | Pellet      | Boil, 5 mins  |

### Yeast

| Name                   | Code    |
| ---------------------- | ------- |
| Wyeast German Ale      | 1007    |

## Brewing Steps

### Mash

* Amount: 21L
* Time: 60 mins
* Strike Temperature: 72°C
* Target Temperature: 67°C

### Sparge

No sparge

### Boil

* Time: 60 mins

## Notes

Great beer. Would brew again like this.
