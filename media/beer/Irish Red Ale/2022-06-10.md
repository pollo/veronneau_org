# Irish Red Ale

An Irish Red Ale, brewed with a Kveik yeast, during the Summer.

## Ingredients

### Fermentables

| Name                                          | Amount  |
|---------------------------------------------- |-------- |
| Innomalt Pale Ale Anglais                     | 4 kg    |
| Maltbroue Cara 40L                            | 200 g   |
| Maltbroue Cara 120L                           | 150 g   |
| Bestmalz Acidulated Malt                      | 200 g   |

### Hops

| Name            | Amount  | Form      | Use           |
|---------------- |-------- | --------- | ------------- |
| Golding         | 20 g    | Pellet    | Boil, 60 mins |
| Golding         | 15 g    | Pellet    | Boil, 10 mins |
| Williamette     | 10 g    | Pellet    | Boil, 5 mins  |
| Williamette     | 20 g    | Pellet    | Dry Hop       |

### Yeast

| Name                                           | Code    |
| ---------------------------------------------- | ------- |
| Escarpment Årset Kveik                         | N/A     |

## Notes

Dry-hopped on 2022-06-13.

Bottled on 2022-06-18.

Opened the first bottle on 2022-06-26. Good head already. Color is spot on, very
red. Very drinkable, the acidity from the acidulated malt comes through nicely.
Maybe could have done with a little less? I would try 100-150g next time instead
of 200g.

Hops flavor is a little strong (spicy? Clearly from the Wiliammette) for my
taste, but it's still very young and it will probably attenuate. Might be worth
replacing it from another variety though.
