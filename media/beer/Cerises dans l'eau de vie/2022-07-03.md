# Récolte

Total: 600g équeutées.

Mauvaise année, fruits touchés par le gel. L'extérieur de plusieurs cerises
est marqué de veines brunes (apparentes sur les fruits verts) qui sont dures et
gardent les fruits petits (frost ring, russetting).

# Ingrédients

* 600g de cerises
* 550mL d'alcool à 40%
* 4 clous de girofle
* 4 piments de jamaique
* 2 tasses de sucre
