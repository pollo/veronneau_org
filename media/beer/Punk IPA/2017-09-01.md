# Funk IPA

Nicknamed 'Funk IPA', this version was inspired by the first two batches.

## Ingredients

### Fermentables

| Name                       | Amount      |
|--------------------------- |------------ |
| 2 row Pale Malt            | 4.3 kg      |
| Caramalt 40L               | 200 g       |

### Hops

| Name            | Amount  | Alpha   | Form      | Use           |
|---------------- |-------- | ------- | --------- | ------------- |
| Cascade         | 13 g    | 7.7%    | Pellet    | Boil, 60 mins |
| Chinook         | 20 g    | 13.8%   | Pellet    | Boil, 60 mins |
| Cascade         | 13 g    | 7.7%    | Pellet    | Boil, 30 mins |
| Chinook         | 20 g    | 13.8%   | Pellet    | Boil, 30 mins |
| Cascade         | 13 g    | 7.7%    | Pellet    | Boil, 10 mins |
| Chinook         | 28 g    | 13.8%   | Pellet    | Boil, 10 mins |
| Citra           | 25 g    | 13.8%   | Pellet    | Boil, 10 mins |
| Cluster         | 40 g    | 8.0%    | Pellet    | Dry Hop       |
| Cascade         | 25 g    | 7.7%    | Pellet    | Dry Hop       |
| Chinook         | 25 g    | 13.8%   | Pellet    | Dry Hop       |
| Belma           | 25 g    | 11.8%   | Pellet    | Dry Hop       |
| Citra           | 15 g    | 13.8%   | Pellet    | Dry Hop       |

### Yeast

| Name                                           | Code    |
| ---------------------------------------------- | ------- |
| White Labs American Ale Yeast Blend            | WLP060  |

## Brewing Steps

### Mash

* Amount: 21L
* Time: 90 mins
* Strike Temperature: 70°C
* Target Temperature: 66°C

### Sparge

* Amount: 5L
* Time: 15 mins
* Strike Temperature: 67°C

### Boil

* Amount: 26L
* Time: 90 mins

## Notes

**OG**: 1.048
**FG**: 1.011

The beer got oxidized, we didn't have a bottling wand yet.
