# Kveik Blond Ale

A Blond Ale trying to be a Czech Lager, brewed with a Kveik strain in the middle
of the summer.

## Ingredients

### Fermentables

| Name                                    | Amount      |
|---------------------------------------- |------------ |
| Innomalt Pilsner Moderne                | 4 kg        |

### Hops

| Name            | Amount  | Form      | Use           |
|---------------- |-------- | --------- | ------------- |
| Saaz            | 25 g    | Pellet    | Boil, 60 mins |
| Saaz            | 10 g    | Pellet    | Boil, 30 mins |
| Saaz            | 10 g    | Pellet    | Boil, 10 mins |
| Saaz            | 15 g    | Pellet    | Boil, 5 mins  |
| Saaz            | 15 g    | Pellet    | Dry Hop       |

### Yeast

| Name                              | Code    |
| ----------------------------------| ------- |
| 2/3 Omega Yeast Lutra Kveik       | OYL-071 |

## Notes

This type of malt is more susceptible to DMS and the mash needs to undergo a
vigorous boil.
