Title: New Desktop Computer
Tags: debian, hardware

I built my last desktop computer what seems like ages ago. In 2011, I was in a
very different place, both financially and as a person. At the time, I was
earning minimum wage at my school's café to pay rent. Since the café was owned
by the school cooperative, I had an employee discount on computer parts. This
gave me a chance to build my first computer from spare parts at a reasonable
price.

After 10 years of service[^1], the time has come to upgrade. Although this
machine was still more than capable for day to day tasks like browsing the web
or playing casual video games, it started to show its limits when time came to
do more serious work.

Old computer specs:

    CPU: AMD FX-8530
    Memory: 8GB DDR3 1600Mhz
    Motherboard: ASUS TUF SABERTOOTH 990FX R2.0
    Storage: Samsung 850 EVO 500GB SATA

I first started considering an upgrade in September 2020: David Bremner was
kindly fixing [a bug][969430] in `ledger` that kept me from balancing my books
and since it seemed like a class of bug that would've been easily caught by an
autopkgtest, I decided to add one.

After adding the necessary snippets to run the upstream testsuite (an easy task
I've done multiple times now), I ran `sbuild` and ...  my computer froze and
crashed. Somehow, what I thought was a simple Python package was maxing all the
cores on my CPU and using all of the 8GB of memory I had available.[^2]

A few month later, I worked on `jruby` and the builds took 20 to 30 minutes —
long enough to completely disrupt my flow. The same thing happened when I
wanted to work on `lintian`: the testsuite would take more than 15 minutes to
run, making quick iterations impossible.

Sadly, the pandemic completely wrecked the computer hardware market and prices
here in Canada have only recently started to go down again. As a result, I had
to wait more time than I would've liked not to pay scalper prices.

New computer specs:

    CPU: AMD Ryzen 5900X
    Memory: 64GB DDR4 3200MHz
    Motherboard: MSI MPG B550 Gaming Plus
    Storage: Corsair MP600 500 GB Gen4 NVME

The difference between the two machines is pretty staggering: I've gone from a
CPU with 2 cores and 8 threads, to one with 12 cores and 24 threads.  Not only
that, but single-threaded performance has also vastly increased in those 10
years.

A good example would be building `grammalecte`, a package [I've recently
sponsored][grammalecte]. I feel it's a good benchmark, since the build relies
on single-threaded performance for the normal Python operations, while being
threaded when it compiles the dictionaries.

On the old computer:

    Build needed 00:10:07, 273040k disk space

And as you can see, on the new computer the build time has been significantly
reduced:

    Build needed 00:03:18, 273040k disk space

Same goes for things like the `lintian` testsuite. Since it's a very
multi-threaded workload, it now takes less than 2 minutes to run; a 750%
improvement.

All this to say I'm happy with my purchase. And — lo and behold — I can now
build `ledger` without a hitch, even though it maxes my 24 threads and uses 28GB
of RAM. Who would've thought...

<img src="/media/blog/2021-06-10/htop_ledger.png" title="Screen capture of htop showing how much resources ledger takes to build" alt="Screen capture of htop showing how much resources ledger takes to build" height="80%" width="80%" style="margin-left: 10%;">

[^1]: I managed to fry that PC's motherboard in 2016 and later replaced it with
  a brand new one. I also upgraded the storage along the way, from a very cheap
  cacheless 120GB SSD to a larger Samsung 850 EVO SATA drive.

[^2]: As it turns out, `ledger` is mostly written in C++ :)

[969430]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=969430
[grammalecte]: https://salsa.debian.org/python-team/packages/grammalecte
