Title: DebConf Videoteam sprint report - day 0
Tags: debconf, debian, sprint, videoteam

First day of the [videoteam autumn sprint][sprint]! Well, I say first day, but
in reality it's more day 0. Even though most of us have arrived in Cambridge
already, we are still missing a few people.

Last year we decided to [sprint in Paris][paris] because most of our video gear
is stocked there. This year, we instead chose to sprint a few days before the
[Cambridge Mini-Debconf][minidc] to help record the conference afterwards.

Since some of us arrived very late and the ones who did arrive early are still
mostly jet lagged (that includes me), I'll use this post to introduce the space
we'll be working from this week and our general plan for the sprint.

## [House Party](https://www.youtube.com/watch?v=UFSyBBglmpI)

After some deliberations, we decided to rent a house for a week in Cambridge: 
finding a work space to accommodate us and all our gear proved difficult and we 
decided mixing accommodation and work would be a good idea.

I've only been here for a few hours, but I have to say I'm pretty impressed by
the airbnb we got. Last time I checked (it seems every time I do, some new room
magically appears), I counted 5 bedrooms, 6 beds, 5 toilets and 3 shower rooms.
Heck, there's even a solarium and a training room with weights and a punching
bag on the first floor.

Having a whole house to ourselves also means we have access to a functional
kitchen. I'd really like to cook at least a few meals during the week.

There's also a cat!

<a href="https://commons.wikimedia.org/wiki/File:Drazet,_a_black_feral_cat.jpg"><img src="/media/blog/2017-11-19/black-cat.jpg" height="100%" width="100%" title="Picture of a black cat I took from Wikipedia. It was too dark outside to use mine" alt="Picture of a black cat I took from Wikipedia. It was too dark outside to use mine"></a>

It's not the house's cat *per say*, but it's been hanging out around the house
for most of the day and makes cute faces trying to convince us to let it come
inside. Nice try cat. Nice try.

Here are some glamour professional photos of what the place looks like on a
perfect summer day, just for the kick of it:

<img src="/media/blog/2017-11-19/exterior.jpg" height="100%" width="100%" title="The view from the garden" alt="The view from the garden">
<img src="/media/blog/2017-11-19/kitchen.jpg" height="100%" width="100%" title="The Kitchen" alt="The Kitchen">
<img src="/media/blog/2017-11-19/bunk-beds.jpg" height="100%" width="100%" title="One of the multiple bedrooms" alt="One of the multiple bedrooms">

Of course, reality has trouble matching all the post-processing filters.

[sprint]: https://wiki.debian.org/Sprints/2017/DebConfVideoteamSprint20Nov2017
[paris]: https://veronneau.org/debconf-videoteam-sprint-day-4.html
[minidc]: https://wiki.debian.org/DebianEvents/gb/2017/MiniDebConfCambridge

## Plan for the week

Now on a more serious note; apart from enjoying the beautiful city of Cambridge,
here's what the team plans to do this week:

### tumbleweed

Stefano wants to continue refactoring our ansible setup. A lot of things have
been added in the last year, but some of it are hacks we should remove and
implement correctly.

### highvoltage

Jonathan won't be able to come to Cambridge, but plans to work remotely, mainly
on our desktop/xfce session implementation. Another pile of hacks waiting to be
cleaned!

### ivodd

Ivo has been working a lot of the pre-ansible part of our installation and plans
to continue working on that. At the moment, creating an installation USB key is
pretty complicated and he wants to make that simpler.

### olasd

Nicolas completely reimplemented our streaming setup for DC17 and wants to
continue working on that.

More specifically, he wants to write scripts to automatically setup and teardown
- via API calls - the distributed streaming network we now use.

Finding a way to push TLS certificates to those mirrors, adding a live stream
viewer on [video.debconf.org](https://video.debconf.org) and adding a viewer to our archive are also
things he wants to look at.

### pollo

For my part, I plan to catch up with all the commits in our ansible repository I
missed since last year's sprint and work on documentation.

It would be very nice if we could have a static website describing our work so
that others (at mini-debconfs for examples) could replicate it easily.

If I have time, I'll also try to document all the ansible roles we have written.

Stay tuned for more daily reports!
