Title: Let's migrate away from GitHub
Tags: git, debian, foss

As many of you heard today, [Microsoft is acquiring GitHub][microsoft]. What
this means for the future of GitHub is not yet clear, but [the folks at
Gitlab][gitlab] think Microsoft's end goal is to integrate GitHub in their
Azure empire. To me, this makes a lot of sense.

Even though I still reluctantly use GitHub for some projects, I migrated all
my personal repositories to Gitlab instances a while ago[^1]. Now is time for
you to do the same and ditch GitHub.

<img src="/media/blog/2018-06-03/ms-lovent-linux.png" title="Microsoft loven't Linux" alt="Microsft loven't Linux" height="25%" width="25%" style="float:right">

Some people might be fine with Microsoft's takeover, but to me it's the straw
that breaks the camel's back. For a few years now, MS has been running a large
marketing campaign on how they love Linux and suddenly decided to embrace Free
Software in all of its forms. More like MS BS to me.

Let us take a moment to remind ourselves that:

* Windows is still a huge proprietary monster that rips billions of people from
  their privacy and rights every day.
* Microsoft is known for spreading FUD about "the dangers" of Free Software in
  order to keep governments and schools from dropping Windows in favor of FOSS.
* To secure their monopoly, Microsoft hooks up kids on Windows by giving out
  "free" licences to primary schools around the world. Drug dealers use the same
  tactics and give out free samples to secure new clients.
* Microsoft's Azure platform - even though it can run Linux VMs - is still a
  giant proprietary hypervisor.

I know moving git repositories around can seem like a pain in the ass, but the
folks at Gitlab are riding the wave of people leaving GitHub and made the the
migration easy [by providing a GitHub importer][importer].

If you don't want to use Gitlab's main instance ([gitlab.org][gitlab_org]), here
are two other alternative instances you can use for Free Software projects:

* The [Debian Gitlab instance][salsa] is available for every FOSS project and
  not only for Debian-related ones[^2]. As long as the project respects the
  [Debian Free Software Guidelines][dfsg], you can use the instance and its CI
  runners.
* Riseup maintains a Gitlab instance for radical projects named [0xacab][]. If
  your [ethos aligns with Riseup's][ethos], chances are they'll be happy to host
  your projects there.

Friends don't let friends use GitHub anymore.

[^1]: Gitlab is pretty good, but it should not be viewed as a panacea: it's
      still an open-core product made by a for-profit enterprise that could one
      day be sold to a large corp like Oracle or Microsoft.
[^2]: See the [Salsa FAQ][faq] for more details.

[microsoft]: https://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github
[gitlab]: https://about.gitlab.com/2018/06/03/microsoft-acquires-github/
[importer]: https://docs.gitlab.com/ee/user/project/import/github.html
[gitlab_org]: https://gitlab.org
[salsa]: https://salsa.debian.org
[dfsg]: https://en.wikipedia.org/wiki/Debian_Free_Software_Guidelines
[faq]: https://wiki.debian.org/Salsa/FAQ#What_can_be_hosted_on_salsa
[0xacab]: https://0xacab.org
[ethos]: https://riseup.net/en/about-us/politics
