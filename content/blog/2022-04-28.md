Title: Montreal's Debian & Stuff - April 2022
Tags: debian, montreal

After two long years of COVID hiatus, local Debian events in Montreal are back!
Last Sunday, nine of us met at [Koumbit][] to work on Debian (and other stuff!),
chat and socialise.

Even though these events aren't always the most productive, it was super fun
and definitely helps keeping me motivated to work on Debian in my spare time.

Many thanks to Debian for providing us a budget to rent the venue for the day
and for the pizzas! Here are a few pictures I took during the event:

<img src="/media/blog/2022-04-28/pizza.jpg" width="70%" style="margin-left:15%" title="Pizza boxes on a wooden bench" alt="Pizza boxes on a wooden bench">

<img src="/media/blog/2022-04-28/board.jpg" width="70%" style="margin-left:15%" title="Whiteboard listing TODO items for some of the participants" alt="Whiteboard listing TODO items for some of the participants">

<img src="/media/blog/2022-04-28/laptops.jpg" width="70%" style="margin-left:15%" title="A table with a bunch of laptops, and LeLutin :)" alt="A table with a bunch of laptops, and LeLutin :)">

If everything goes according to plan, our next meeting should be sometime in
June. If you are interested, the best way to stay in touch is either to
[subscribe to our mailing list][list] or to join our IRC channel
(#debian-quebec on OFTC). Events are also posted on Quebec's [Agenda du
libre][agenda].

[koumbit]: https://www.koumbit.org/en
[list]: https://lists.debian.org/debian-dug-quebec/
[agenda]: https://agendadulibre.qc.ca/tags/debian-qu%C3%A9bec
