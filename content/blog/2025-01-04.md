Title: Montreal's Debian & Stuff - December 2024
Tags: debian, montreal

Our Debian User Group met on December 22<sup>nd</sup> for our last meeting of
2024. I wasn't sure at first it was a good idea, but many people showed up and
it was great!

Here's what we did:

**pollo:**

 * migrated to a new Yubikey
 * merged a [lintian MR][lintian]

**anarcat:**

 * fought with the Supersonic flatpak to [fix build with latest
   placebo][supersonic] (failed), but managed to update to the latest upstream
 * realized that [keyring-pass][] does the inverse of what he needs, whereas
   [pass_secret_service][], which does, is poorly maintained  and depends on
   the dead pypass library
 * uploaded new upstream versions of `etckeeper`, `mdformat` and
   `python-internetarchive`
 * [improved a lintian warning][lintian]
 * opened yet another [sbuild bug][1091169]
 * started playing with [sfwbar][]

**lelutin:**

 * worked on the [2024 Advent of Code][advent] in Rust

**lavamind:**

 * installed Debian on an oooollld (as in, with a modem) laptop
 * debugged a [FTBFS on jruby][1091127]

**tvaz:**

 * did some simple packaging QA
 * added basic salsa CI and some RFA for a bunch of packages
   (`python-midiutil`, `antimony`, `python-pyo`, `rakarrack`, `python-pyknon`,
   `soundcraft-utils`, `cecilia`, `nasty`, `gnome-icon-theme-nuovo`,
   `gnome-extra-iconsg`, `nome-subtitles`, `timgm6mb-soundfont`)

**mjeanson and joeDoe:**

 * hanged out and did some stuff :)

Some of us ended up grabbing a drink after the event at [l'Isle de Garde][pub],
a pub right next to the venue.

[supersonic]: https://github.com/flathub/io.github.dweymouth.supersonic/pull/114
[keyring-pass]: https://github.com/nazarewk/keyring_pass
[pass_secret_service]: https://github.com/mdellweg/pass_secret_service/
[lintian]: https://salsa.debian.org/lintian/lintian/-/merge_requests/547
[1091169]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1091169
[sfwbar]: https://github.com/LBCrion/sfwbar
[advent]: https://adventofcode.com/
[1091127]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1091127
[pub]: https://www.isledegarde.com/

## Pictures

This time around, we were hosted by [l'Espace des possibles][espace], at their
new location (they moved since our last visit). It was great! People liked the
space so much we actually discussed going back there more often :)

<img src="/media/blog/2025-01-04/espace.jpg" width="70%" style="margin-left:15%" title="Group photo at l'Espace des possibles" alt="Group photo at l'Espace des possibles">

[espace]: https://solon-collectif.org/action/espace-des-possibles-petite-patrie/
