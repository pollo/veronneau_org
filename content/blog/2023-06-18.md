Title: Solo V2: nice but flawed
Tags: u2f, hardware

**EDIT:** One of my 2 keys has died. There are what seems like golden bubbles
under the epoxy, over one of the chips and those were not there before. I've
emailed SoloKeys and I'm waiting for a reply, but for now, I've stopped using
the Solo V2 altogether :(

I recently received the two Solo V2 hardware tokens I ordered as part of their
crowdfunding campaign, back in March 2022. It did take them longer than
advertised to ship me the tokens, but that's hardly unexpected from such
small-scale, crowdfunded undertaking.

I'm mostly happy about my purchase and I'm glad to get rid of the aging [Tomu
boards][tomu] I was using as U2F tokens[^fido]. Still, beware: I am not sure
it's a product I would recommend if what you want is simply something that
works. If you do not care about open-source hardware, the Solo V2 is not for
you.

[^fido]: Although U2F is still part of the FIDO2 specification, the Tomus
    predate this standard and were thus not fully compliant with FIDO2. So long
    and thanks for all the fish little boards, you've served me well!

[tomu]: https://veronneau.org/i-am-tomu.html

## The Good

<img src="/media/blog/2023-06-18/side_by_side.jpg" width="70%" style="margin-left:15%"  title="A side-by-side view of the Solo V2's top and back sides" alt="A side-by-side view of the Solo V2's top and back sides">

I first want to mention I find the Solo V2 gorgeous. I really like the black and
gold color scheme of the USB-A model (which is reversible!) and it seems like a
well built and solid device. I'm not afraid to have it on my keyring and I fully
expect it to last a long time.

<img src="/media/blog/2023-06-18/solo2_shell.webp" width="70%" style="margin-left:15%"  title="An animation of the build process, showing how the PCB is assembled and then slotted into the shell" alt="An animation of the build process, showing how the PCB is assembled and then slotted into the shell">

I'm also very impressed by the modular design: the PCB sits inside a shell,
which decouples the logic from the USB interface and lets them manufacture a
single board for both the USB-C and USB-A models. The clear epoxy layer on top
of the PCB module also looks very nice in my opinion.

<img src="/media/blog/2023-06-18/capacitive.jpg" width="70%" style="margin-left:15%" title="A picture of the Solo V2 with its silicone case on my keyring, showing the 3 capacitive buttons" alt="A picture of the Solo V2 with its silicone case on my keyring, showing the 3 capacitive buttons">

I'm also very happy the Solo V2 has capacitive touch buttons instead of
physical "clicky" buttons, as it means the device has no moving parts. The
token has three buttons (the gold metal strips): one on each side of the device
and a third one near the keyhole.

As far as I've seen, the FIDO2 functions seem to work well via the USB
interface and do not require any configuration on a Debian 12 machine. I've
already migrated to the Solo V2 for web-based 2FA and I am in the process of
migrating to an SSH `ed25519-sk` key. [Here is a guide][frehi] I recommend if
you plan on setting those up with a Solo V2.

[frehi]: https://blog.frehi.be/2022/08/04/using-the-solo-v2-fido2-security-key/

## The Bad and the Ugly

Sadly, the Solo V2 is far from being a perfect project. First of all, since the
crowdfunding campaign is still being fulfilled, it is not currently
commercially available. Chances are you won't be able to buy one directly
before at least Q4 2023.

I've also hit what seems to be a pretty big firmware bug, or at least, one that
affects my use case quite a bit. Invoking `gpg` crashes the Solo V2 completely
if you also have `scdaemon` installed. Since `scdaemon` is necessary to use
`gpg` with an OpenPGP smartcard, this means you cannot issue any `gpg` commands
(like signing a git commit...) while the Solo V2 is plugged in.

Any `gpg` commands that queries `scdaemon`, such as `gpg --edit-card` or `gpg
--sign foo.txt` times out after about 20 seconds and leaves the token
unresponsive to both touch and CLI commands.

The way to "fix" this issue is to make sure `scdaemon` does not interact with
the Solo V2 anymore, using the `reader-port` argument:

1. Plug both your Solo V2 and your OpenPGP smartcard

1. To get a list of the tokens `scdaemon` sees, run the following command: `$
   echo scd getinfo reader_list | gpg-connect-agent --decode | awk '/^D/ {print
   $2}'`

1. Identify your OpenPGP smartcard. For example, my Nitrokey Start is listed as
   `20A0:4211:FSIJ-1.2.15-43211613:0`

1. Create a file in `~/.gnupg/scdaemon.conf` with the following line
   `reader-port $YOUR_TOKEN_ID`. For example, in my case I have: `reader-port
   20A0:4211:FSIJ-1.2.15-43211613:0`

1. Reload `scdaemon`: `$ gpgconf --reload scdaemon`

Although this is clearly a firmware bug[^trussed], I do believe GnuPG is also
partly to blame here. Let's just say I was not very surprised to have to battle
`scdaemon` again, as I've had [previous issues with it][scdaemon].

Which leads me to my biggest gripe so far: it seems SoloKeys (the company)
isn't really fixing firmware issues anymore and doesn't seems to care. The last
firmware release is about a year old.

Although people are experiencing serious bugs, there is [no official way to
report them][closed], which leads to issues being seemingly ignored. For
example, [the NFC feature is apparently killing keys][NFC] (!!!), but no one
from the company seems to have acknowledged the issue. The same goes for my
GnuPG bug, which [was flagged in September 2022][share].

For a project that mainly differentiates itself from its (superior) competition
by being "Open", it's not a very good look... Although “*SoloKeys is still an
unprofitable open source side business of its creators*”[^side], this kind of
attitude certainly doesn't help foster trust.

[^trussed]: It appears the Solo V2 [shares its firmware][share] with the
    Nitrokey 3, which had [a similar issue][nitrokey] a while back.
[^side]: This is a [direct quote][closed] from one of the Solo V2 firmware
    maintainers.

[scdaemon]: https://veronneau.org/preventing-an-openpgp-smartcard-from-caching-the-pin-eternally.html
[closed]: https://github.com/solokeys/solo2/discussions/124#discussioncomment-3584059
[NFC]: https://github.com/solokeys/solo2/discussions/138
[share]: https://github.com/solokeys/solo2/discussions/141#discussion-4423876
[nitrokey]: https://github.com/Nitrokey/nitrokey-3-firmware/issues/22

## Conclusion

If you want to have a nice, durable FIDO2 token, I would suggest you get one of
the many models Yubico offers. They are similarly priced, are readily
commercially available, are part of a nice and maintained software ecosystem
and have more features than the Solo V2 (OpenPGP support being the one I miss
the most). Yubikeys are the *practical* option.

What they are not is open-source hardware, whereas [the Solo V2 is][oshwa]. As
bunnie [very well explained on his blog in 2019][bunnie], it does not mean
the later is inherently more trustable than the former, but it does make the
Solo V2 the *ideological* option. Knowledge is power and it should be free.

As such, tread carefully with SoloKeys, but don't dismiss them altogether: the
Solo V2 is certainly functioning well enough for me.

[bunnie]: https://www.bunniestudios.com/blog/?p=5706
[oshwa]: https://certification.oshwa.org/us001100.html
