Title: Taiwan Travel Blog - Day 1
Tags: travel, taiwan

I'm going to [DebConf18][dc18] later this month, and since I had some free time
and I speak a somewhat understandable mandarin, I decided to take a full month
of vacation in Taiwan.

I'm not sure if I'll keep blogging about this trip, but so far it's been very
interesting and I felt the urge to share the beauty I've seen with the world.

This was the first proper day I spent in Taiwan. I arrived on the 8th during the
afternoon, but the time I had left was all spent traveling to Hualien County
(花蓮縣) were I intent to spend the rest of my time before DebConf.

## Language Rant

I'm mildly annoyed at Taiwan for using traditional Chinese characters instead
of simplified ones like they do in Mainland China. So yeah, even though I've
been studying mandarin for a while now, I can't read much if anything at all.
For those of you not familiar with mandarin, here is an example of a very common
character written with simplified (后) and traditional characters (後). You
don't see the resemblance between the two? Me neither.

I must say technology is making my trip much easier though. I remember a time
when I had to use my pocket dictionary to lookup words and characters and it
used to take me up to 5 minutes to find a single character[^1]. That's how you
end up ordering cold duck blood soup from a menu without pictures after having
given up on translating it.

Now, I can simply use my smartphone and draw the character I'm looking for in my
dictionary app. It's fast, it's accurate and it's much more complete than a
small pocket dictionary.

## Takoro National Park (太鲁阁国家公园)

Since I've seen a bunch of large cities in China already and I both dislike
pollution and large amounts of people squished up in too few square meters, I
rapidly decided I wasn't going to visit Taipei and would try to move out and
explore one of the many national parks in Taiwan.

After looking it up, Takoro National Park in the Hualien County seemed the best
option for an extended stay. It's large enough that there is a substantial
tourism economy built around visiting the multiple trails of the park, there are
both beginner and advanced trails you can choose from and the scenery is
incredible.

Also Andrew Lee lives nearby and had a bunch of very nice advice for me, making
my trip to Takoro much easier.

### Swallow Gorge (燕子口)

<img src="/media/blog/2018-07-09/yanzikou.jpg" title="Picture of the LiWu river in Yanzikou" alt="Picture of the LiWu river in Yanzikou" height="30%" width="30%" style="float:left">

The first trail I visited in the morning was Swallow Gorge. Apparently it's
frequently closed because of falling rocks. Since the weather was very nice and
the trail was open, I decided to start by this one.

Fun fact, at first I thought the *swallow* in Swallow Gorge meant *swallowing*,
but it is *swallow* as in the cute bird commonly associated with spring time.
The gorge is named that way because the small holes in the cliffs are used by
swallows to nest. I kinda understood that when I saw a bunch of them diving and
playing in the wind in front of me.

The Gorge was very pretty, but it was full of tourists and the "trail" was
actually a painted line next to the road where car drives. It was also pretty
short. I guess that's ok for a lot of people, but I was looking for something a
little more challenging and less noisy.

### Shakadang Trail (砂卡礑步道)

The second trail I visited was the Shakadang trail. The trail dates back to
1940, when the Japanese tried to use the Shakadang river for hydroelectricity.

<img src="/media/blog/2018-07-09/blue_water.jpg" title="Shakadang's river water is bright blue and extremely clear" alt="Shakadang's river water is bright blue and extremely clear" height="30%" width="30%" style="float:right">

This trail was very different from Yanzikou, being in the wild and away from
cars. It was a pretty easy trail (2/5) and although part of it was paved with
concrete, the more you went the wilder it got. In fact, most of the tourist gave
up after the first kilometer and I had the rest of the path to myself
afterwards.

<img src="/media/blog/2018-07-09/purple_plant.jpg" title="Some cute purple plant growing along the river" alt="Some cute purple plant growing along the river" height="30%" width="30%" style="float:left">

The path is home to a variety of wild animals, plants and insects. I didn't see
any wild board, but gosh damn did I saw some freakingly huge spiders. As I
learnt later, Taiwan is home of the largest spiders in the world. The ones I saw
(Golden silk orb-weaver, [*Nephila pilipes*][spider]) had bodies easily 3 to 5cm
long and 2cm thick, with an overall span of 20cm with their legs.

I also heard some bugs (I guess it was bugs) making a huge racket that somewhat
reminded me of an old car's loose alternator belt strap on a cold winter
morning.

<audio controls><source src="/media/blog/2018-07-09/forest_sound.mp3" type="audio/mpeg"></audio>

[dc18]: https://debconf18.debconf.org
[spider]: https://en.wikipedia.org/wiki/Nephila_pilipes
[^1]: Using a Chinese dictionary is a hard thing to do since there is no
      alphabet. Instead, the characters are classified by the number of strokes
      in their radicals and then by the number of strokes in the rest of the
      character.
