Title: Holiday Beer Recipe - Le Courant Noir
Tags: beer, recipes

It's holiday season once again, and while I'm waiting for the deserts I made for
my family's Christmas party to finish cooking (I highly recommend *Bon Apétit*'s
[Brûléed Bourbon-Maple Pumpkin Pie][pie]), I opened one of the beers I brewed
recently.

And oh boy, what a success. 

I've been brewing beer with 2 other friends for a few years now, and while we've
brewed some excellent stuff in the past, I feel *Le Courant Noir*<sup>[1](#note1)</sup>
- a blackcurrant witbier-inspired ale - is my most resounding achievement.

This was my first time brewing with fresh fruits, and I'm very happy with the
results. The beer has a very pleasant, sharp nose of blackcurrants and esters.
To the taste, the blackcurrant comes through, but is counterbalanced by the malt
and pretty high alcohol content (~8% ABV). The result is a tart, ever so
slightly acidic fruity beer. I love it.

<img src="/media/blog/2017-12-24/courant_noir.jpg" title="A glass of Courant Noir" alt="A glass of Courant Noir" height="30%" width="30%" style="float:right">

So yeah, I thought I'd share the recipe in case you want to try replicating it.
Try to get fresh blackcurrant, as what you are looking for is the tart taste of
the blackcurrant. Using syrup, you're bound to get some jelly-like aftertaste.

[pie]: https://www.bonappetit.com/recipe/bruleed-bourbon-maple-pumpkin-pie

## Recipe

The target boil volume is 25L and the target batch size 20L. I'm mashing with a
pretty low efficiency (70%), so if you use a proper mash tun, you might want to
use a little less grain.

Mash at 67°C and ferment at 19°C. Add the blackcurrants whole once the primary
fermentation is over.

Malt:

 * 2.8 kg x 2 row Pale Malt
 * 2.8 kg x White Wheat Malt
 * 1.0 kg x Munich Malt

Hops:

 * 35 g x Saaz (4.4% alpha acid) - 60 min Boil
 * 25 g x Saaz (4.4% alpha acid) - 30 min Boil
 * 15 g x Saaz (4.4% alpha acid) - Dry Hop

Yeast:

 * White Labs Belgian Witbier Ale Yeast - WLP400

Other:

 * 25 g x Coriander Seeds (crushed) - 10 min Boil
 * 1.7 kg x Whole Blackcurrant

## Pie

Here's a bonus picture of the pie I referenced earlier.

<img src="/media/blog/2017-12-24/pumpkin_pie.jpg" title="Pumpkin pie in the oven in a cast iron pan" alt="Pumpkin pie in the over in a cast iron pan" height="100%" width="100%">
______

<p style="font-size:95%;"><a name="note1">1</a> - Amongst other things, "courant noir"
is the French word-for-word translation for blackcurrant. It's also a very bad
translation pun Ⓐ ⚑.</p>
