Title: Holiday cookies
Tags: recipes,food

Holiday season once again! Each and every year, I struggle with the gift ritual
in my family. Everyone (especially my closest relatives) feel obligated to find
some special gifts to give and spend a lot of money on that. I loathe it.

Either it ends up being some useless crap I'll never use or it's stuff so
expensive it makes me feel bad about it. Anyway, I used to simply not give
anything in return and mumble, but I'm now at an age where I'm supposed to start
taking part in this ritual.

So this year, instead of joining the general capitalist frenzy that everyone
partakes in, I decided to bake cookies for my loved ones. Cookies are nice,
everyone likes them, they are relatively cheap and rely less on exploiting
people in the global South than your typical gifts.

# Honey Oatmeal Cookies

I received a lot of honey at the end of the summer from a friend who has beehives
and I don't really like honey by itself. I prefer the taste of maple syrup.
But honey being thicker and having a stronger flavour, it makes a great way to
replace processed sugar in a lot of recipes.

I also wanted the cookies to last (we eat enough baked goods during the Holidays
as it is), so I wanted them to be hard cookies. He's the recipe I used:

<img src="/media/blog/2017-01-01/cookies.jpg" height="100%" width="100%" title="Homemade cookies in a cookie tin" alt="Homemade cookies in a cookie tin">

## Ingredients

This recipe makes around 50 big cookies. Sorry for the derp units, it's quite
standard for recipes here...

* 3 cups salted Butter
* 2 2/3 cups Honey
* 8 cups Oats
* 3 1/2 cups whole Flour
* 3 teaspoons Baking Powder

## Baking steps

**1 -** Melt the butter until fluid. Remove from the heat and add the honey.

**2 -** Stir in the oats and baking powder.

**3 -** Stir the flour in.

**4 -** Grease a cookie sheet and flatten balls of dough into 2 inches cookies
        (the cookies won't expand or change shape a lot during the baking
        process, so make them the size you want them to be).

**5 -** Bake at 375°F for around 35 minutes. This time depends a lot on the size
        of cookies you made, so watch your first batch and iterate from there.
        Your cooked cookies should be golden and hard.

# Preserved eggs

In October 2016, I [made pickled beets](/for-the-love-of-goddamn-pickles.html) and had a lot of
liquid left from the process.

I has previously used pickle juice to make preserved eggs, so I decided to use
some of the pickled beets juice this way!

<img src="/media/blog/2017-01-01/eggs.jpg" height="100%" width="100%" title="Homemade cookies in a cookie tin" alt="Homemade cookies in a cookie tin">

A dozen eggs fit nicely in a 1L mason jar and the eggs were ready after
approximately 1 week. Don't throw your pickle juices!
