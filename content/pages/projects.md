Title: Projects
status: hidden
save_as: projects

# Projects

Here is a list of my pet projects:

## Debian

I've been a Debian user since 2010 and a Debian Developer since 2018! Have a
look at my [Package Overview][DDPO] page to see what I'm currently working on.

[DDPO]: https://qa.debian.org/developer.php?login=pollo&comaint=yes

## DebConf Video Team

The Debian project has an annual conference called DebConf and I've been
involved in the team capturing the conference (the DebConf videoteam) since
2015.

Check out our [documentation][doc] and our [tools][tools].

[doc]: https://debconf-video-team.pages.debian.net/docs/
[tools]: https://salsa.debian.org/debconf-video-team

## Tor

I have been running a Tor bridge included in the TBB default list [since
2018][2018]. The first bridge I ran was smallerRichard, using spare
resources at work. This bridge was eventually take down after I left.

The bridge I currently maintain, [PraxedisGuerrero][], has been running [since
late 2020][2020]. It is a VPS I pay with my own money and I manage it with [this
puppet module][puppet-tor] I also maintain.

Tor is a great project and I'm happy to be able to contribute to the stability
and ease of access of the network.

[2018]: https://gitweb.torproject.org/builders/tor-browser-build.git/commit/?id=03883ab8278cbc49deee0ee3285a2de4d215a929
[smallerRichard]: https://metrics.torproject.org/rs.html#details/7171D44C4CEB16D4972ABAC43E199D03FADFD679
[PraxedisGuerrero]: https://metrics.torproject.org/rs.html#details/B35B0E377EBB633C926FB93D81749DB01A030A44
[2020]: https://gitweb.torproject.org/builders/tor-browser-build.git/commit/?id=497972c69e87897d7b83c15d78e359794f9bad4f
[puppet-tor]: https://gitlab.com/shared-puppet-modules-group/tor

## Beer

I've been homebrewing with friends since Fall 2014. You can find my beer recipes
(it's mostly a personal beer journal) [here][beer].

[beer]: /media/beer/

## My bicycles

I love bicycles. They are beautiful machines and are part of a different way of
seeing cities.

I've been riding bikes for a while now, 4 seasons a year. Hell yeah, winter
biking.

My current summer bike has:

* a Brooks B17 Narrow saddle
* a pair of Mavic A719 36 spokes 700C wheels with a Shimano Deore XT dynamo
  front wheel
* Schwalbe Marathon Plus 35mm tires
* a Busch & Müller Lumotec IQ Premium Fly RT Senso Plus front light, to go with
  the dynamo
* a pair of Velo Orange hammered aluminium mudguards
* a Velo Orange Brass Striker Bell (bells are cool!!)
* Tektro RL720 and RL520 brake levers
* Microshift indexed bar end shifters
* a Shimano Deore XT 9 speed rear derailleur
* a Shimano Deore Hollowtech II 3 speeds crank (48, 36, 26)
* a Shimano 9 speed "mountain" cassette
* SPD pedals and SIDI SPD shoes

Here's a picture of it:

<img src="/media/summer_bike.jpg" width="70%" style="margin-left:15%" title="A picture of my summer bike" alt="A picture of my summer bike">

I also have a winter bicycle built on a Surly Cross-Check frame with a Sturmey
Archer 3 speeds rear hub and Schwalbe Marathon Winter 700C tires.

<img src="/media/blog/2021-06-30/new_winter_bike.jpg" width="70%" style="margin-left:15%" title="A picture of my winter bike" alt="A picture of my winter bike">
