Title: Home
status: hidden
save_as: index.html

# Who am I?

My name is Louis-Philippe Véronneau and I am an Economist specialising in Free
Software and a Debian Developer. These days, I teach Economics to college
students in Montréal, Canada.

# Code

All the projects I work on and find important are on my [project page][projects].

You can find all the projects I contribute to on [Gitlab][gitlab] and on
[Github][github]. I try to host most of my code on Gitlab, but sadly Github
seems to be a must for some bigger projects.

This website runs on [Pelican][pelican], a free software static blog generator
in Python. You can find the code behind this website [here][code].

[projects]: /projects
[gitlab]: https://gitlab.com/baldurmen
[github]: https://github.com/baldurmen
[pelican]: https://blog.getpelican.com/
[code]: https://0xacab.org/pollo/veronneau_org.git

# Contact

 Louis-Philippe Véronneau, a.k.a pollo

 **Email:** &#112;&#111;&#108;&#108;&#111;&#064;&#100;&#101;&#098;&#105;&#097;&#110;&#046;&#111;&#114;&#103;

 **GPG:** F64D 61D3 21F3 CB48 9156  753D E1E5 457C 8BAD 4113
