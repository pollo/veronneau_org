#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Louis-Philippe V\xe9ronneau'
SITENAME = u'Louis-Philippe V\xe9ronneau'
SITEURL = 'https://veronneau.org'

PATH = 'content'
STATIC_PATHS = []
THEME = 'theme'
MENUITEMS = (('Home', '/'),
             ('Projects', '/projects'),
             ('Blog', '/blog'))

TIMEZONE = 'America/New_York'
LOCALE = 'en_CA.utf8'
DEFAULT_PAGINATION = 6
DEFAULT_LANG = u'en'

# Override or disable default pages
INDEX_SAVE_AS = 'blog'
CATEGORY_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
ARCHIVES_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
TAGS_SAVE_AS = ''

# Feeds
FEED_MAX_ITEMS = 20
FEED_ALL_ATOM = 'feeds/atom.xml'
TAG_FEED_ATOM = 'feeds/tags/{slug}.atom.xml'
TRANSLATION_FEED_ATOM = 'feeds/languages/{lang}.atom.xml'
AUTHOR_FEED_ATOM = 'feeds/authors/{slug}.atom.xml'
CATEGORY_FEED_ATOM = None
AUTHOR_FEED_RSS = None
