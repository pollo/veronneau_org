This is the source code for my personal blog, hosted on https://veronneau.org.

## Usage

To build the static site, you will need pelican:

    $ sudo apt install pelican

Once you have pelican installed, you can generate the static website this way:

    $ pelican content
